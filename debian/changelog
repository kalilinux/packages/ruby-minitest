ruby-minitest (5.3.3-1kali1) kali; urgency=medium

  * Imported debian package 5.3.3-1
  * debian/control: gem2deb downgraded to 0.3.0
  * Ignore tests on ruby 1.8 

 -- Sophie Brun <sophie@freexian.com>  Fri, 30 May 2014 09:58:29 +0200

ruby-minitest (5.3.3-1) unstable; urgency=medium

  * Imported Upstream version 5.3.3

 -- Cédric Boutillier <boutil@debian.org>  Tue, 15 Apr 2014 21:42:10 +0200

ruby-minitest (5.3.0-1) unstable; urgency=medium

  * Imported Upstream version 5.3.0

 -- Cédric Boutillier <boutil@debian.org>  Fri, 28 Feb 2014 14:27:26 +0100

ruby-minitest (5.2.1-1) unstable; urgency=medium

  * Imported Upstream version 5.2.1

 -- Cédric Boutillier <boutil@debian.org>  Fri, 10 Jan 2014 20:16:37 +0100

ruby-minitest (5.1.0-1) unstable; urgency=medium

  * Imported Upstream version 5.1.0
  * debian/control: 
    + bump Standards-Version to 3.9.5 (no changes needed)
    + build with gem2deb >= 0.5 to drop ruby1.8 support
    + update the homepage field
  * debian/patches:
    + refresh disable-some-tests and disable-require-rubygems
    + drop load_plugins_ruby18.patch, not needed anymore

 -- Cédric Boutillier <boutil@debian.org>  Mon, 09 Dec 2013 16:15:29 +0100

ruby-minitest (5.0.5-1) unstable; urgency=low

  * New upstream version

 -- Cédric Boutillier <boutil@debian.org>  Wed, 26 Jun 2013 09:34:04 +0200

ruby-minitest (5.0.4-1) unstable; urgency=low

  * New upstream version
  * Install upstream changelog
  * debian/patches:
    + load_plugins_noop_ruby18.patch renamed load_plugins_ruby18.patch.
      It allows now to load plugins in $LOAD_PATH with Ruby 1.8, without
      rubygems.
    + disable-some-tests patch refreshed

 -- Cédric Boutillier <boutil@debian.org>  Wed, 12 Jun 2013 16:38:53 +0200

ruby-minitest (5.0.3-1) unstable; urgency=low

  * New upstream version

 -- Cédric Boutillier <boutil@debian.org>  Tue, 04 Jun 2013 16:47:30 +0200

ruby-minitest (5.0.1-2) unstable; urgency=low

  * update load_plugins_noop_ruby18.patch to make it work with Ruby
    librairies using specific parts of rubygems

 -- Cédric Boutillier <boutil@debian.org>  Thu, 16 May 2013 16:14:13 +0200

ruby-minitest (5.0.1-1) unstable; urgency=low

  * New upstream version
  * debian/patches:
    + refresh disable-require-rubygems and disable-some-tests
    + add load_plugins_noop_ruby18.patch to deactivate load_plugins with
      Ruby1.8, and without rubygems
  * add 'unapply-patches' to debian/source/local-options

 -- Cédric Boutillier <boutil@debian.org>  Thu, 16 May 2013 01:09:24 +0200

ruby-minitest (4.7.4-1) unstable; urgency=low

  * New upstream release
  * Upload to unstable

 -- Cédric Boutillier <boutil@debian.org>  Mon, 06 May 2013 14:44:57 +0200

ruby-minitest (4.7.3-1) experimental; urgency=low

  * New upstream release 

 -- Cédric Boutillier <boutil@debian.org>  Wed, 24 Apr 2013 17:39:35 +0200

ruby-minitest (4.7.0-1) experimental; urgency=low

  * New upstream version
  * Refresh debian/patches/disable-some-tests
  * debian/control:
    - remove DM-Upload-Allowed: field
    - change my email address in Uploaders:
    - change Vcs-* fields to use anonscm.debian.org
    - replace ruby1.8 by ruby in Depends
  * debian/copyright:
    - update source URL
    - update years in copyright information
    - change my email address

 -- Cédric Boutillier <boutil@debian.org>  Sat, 06 Apr 2013 13:47:56 +0200

ruby-minitest (3.2.0-1) unstable; urgency=low

  * New upstream version

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Wed, 27 Jun 2012 11:23:55 +0200

ruby-minitest (3.1.0-2) unstable; urgency=low

  * Bump build dependency on gem2deb to >= 0.3.0~

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Tue, 26 Jun 2012 13:14:31 +0200

ruby-minitest (3.1.0-1) unstable; urgency=low

  * New upstream version

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Wed, 20 Jun 2012 19:25:01 +0200

ruby-minitest (3.0.1-1) unstable; urgency=low

  * New upstream version

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Tue, 12 Jun 2012 18:33:50 +0200

ruby-minitest (3.0.0-1) unstable; urgency=low

  * New upstream version
  * Refresh debian/patch/disable-some-tests patch

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Thu, 10 May 2012 14:24:59 +0200

ruby-minitest (2.12.1-1) unstable; urgency=low

  * New upstream version
  * Refresh debian/patch/disable-some-tests patch

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Sun, 22 Apr 2012 14:05:16 +0200

ruby-minitest (2.11.4-1) unstable; urgency=low

  * New upstream version
  * Refresh patch 'disable-some-tests'
  * Add myself to Uploaders
  * Bump Standards-Version to 3.9.3 (no changes needed)
  * Update Format URL for DEP5 debian/copyright and rename license
    from MIT to Expat

 -- Cédric Boutillier <cedric.boutillier@gmail.com>  Thu, 29 Mar 2012 22:06:59 +0200

ruby-minitest (2.9.1-1) unstable; urgency=low

  * New upstream release.

 -- Lucas Nussbaum <lucas@debian.org>  Tue, 20 Dec 2011 20:26:32 +0100

ruby-minitest (2.6.1-1) unstable; urgency=low

  * New upstream version.
  * Switch to my @d.o email address.
  * Standards-Version -> 3.9.2. No changes needed.
  * Switch test runner.
  * Fix debian/copyright. Thanks lintian.
  * disable-some-tests: Refresh patch.
  * Add patch: disable-require-rubygems.

 -- Lucas Nussbaum <lucas@debian.org>  Sun, 09 Oct 2011 13:26:35 +0200

ruby-minitest (2.1.0-1) unstable; urgency=low

  * Initial release.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Wed, 27 Apr 2011 21:44:04 +0200
